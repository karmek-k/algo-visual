import '@fontsource/noto-sans';
import './App.css';
import Heading from './components/Heading';
import SortingVisualization from './components/sorting/SortingVisualization';

function App() {
  return (
    <>
      <Heading header="Algo Visual" subheader="Sorting algorithms visualized" />
      <SortingVisualization />
    </>
  );
}

export default App;
