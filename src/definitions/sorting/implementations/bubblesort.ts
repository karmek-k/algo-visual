import { Inputs, VisualizationContext } from '../types';

export async function bubblesort(
  inputs: Inputs,
  ctx: VisualizationContext
): Promise<void> {
  for (let i = 0; i < inputs.length - 1; ++i) {
    for (let j = 0; j < inputs.length - 1; ++j) {
      // highlight `j` and `j + 1` and set default color for others
      // TODO: fix this
      inputs = inputs.map((input, idx) =>
        idx === j || idx === j + 1
          ? { color: '#A0A', ...input }
          : { color: undefined, ...input }
      );
      await ctx.update(inputs);

      if (inputs[j + 1].number < inputs[j].number) {
        const temp = inputs[j + 1];
        inputs[j + 1] = inputs[j];
        inputs[j] = temp;
      }

      await ctx.update(inputs);
    }
  }

  return Promise.resolve();
}
