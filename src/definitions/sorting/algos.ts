import { AlgoImplementation } from './types';
import { bubblesort } from './implementations/bubblesort';

export interface SortingAlgoData {
  codeName: string;
  name: string;
  implementation: AlgoImplementation;
}

export const algos: SortingAlgoData[] = [
  {
    codeName: 'bubblesort',
    name: 'Bubblesort',
    implementation: bubblesort,
  },
  {
    codeName: 'mergesort',
    name: 'Mergesort',
    implementation: async () => {},
  },
  {
    codeName: 'bogosort',
    name: 'Bogosort',
    implementation: async () => {},
  },
];
