export type Input = { number: number; color?: string };
export type Inputs = Input[];
export type AlgoImplementation = (
  inputs: Inputs,
  visual: VisualizationContext
) => Promise<void>;

export interface VisualizationContext {
  update(values: Inputs): Promise<void>;
}
