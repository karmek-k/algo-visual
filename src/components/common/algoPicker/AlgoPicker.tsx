import { SortingAlgoData } from '../../../definitions/sorting/algos';
import useAlgoPicker from './useAlgoPicker';
import './AlgoPicker.css';
import { useEffect } from 'react';

type Props = {
  algos: SortingAlgoData[];
  setAlgo: React.Dispatch<React.SetStateAction<SortingAlgoData | null>>;
};

export default function AlgoPicker({ algos, setAlgo }: Props) {
  const { components, current } = useAlgoPicker(algos);

  useEffect(() => {
    setAlgo(current);
  }, [current]);

  return <div className="algo_picker">{components}</div>;
}
