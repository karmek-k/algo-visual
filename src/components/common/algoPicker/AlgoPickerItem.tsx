import { SortingAlgoData } from '../../../definitions/sorting/algos';

interface Props {
  isSelected?: boolean;
  data: SortingAlgoData;
  dispatcher: React.Dispatch<React.SetStateAction<SortingAlgoData | null>>;
}

export default function AlgoPickerItem({
  isSelected,
  data,
  dispatcher,
}: Props) {
  const className = `algo_picker__algo ${
    isSelected && 'algo_picker__algo--selected'
  }`;

  return (
    <button className={className} onClick={() => dispatcher(data)}>
      {data.name}
    </button>
  );
}
