import { useEffect, useState } from 'react';
import { SortingAlgoData } from '../../../definitions/sorting/algos';
import AlgoPickerItem from './AlgoPickerItem';

export default function useAlgoPicker(algos: SortingAlgoData[]) {
  const [current, setCurrent] = useState<SortingAlgoData | null>(null);
  const [components, setComponents] = useState<JSX.Element[]>([]);

  useEffect(() => {
    setComponents(
      algos.map(algo => (
        <AlgoPickerItem
          key={algo.codeName}
          isSelected={algo.codeName === current?.codeName}
          data={algo}
          dispatcher={setCurrent}
        />
      ))
    );
  }, [current]);

  return { components, current };
}
