import './Heading.css';

interface Props {
  header: string;
  subheader?: string;
}

export default function Heading(props: Props) {
  return (
    <header className="heading">
      <h1 className="heading__header">{props.header}</h1>
      {props.subheader && (
        <h2 className="heading__subheader">{props.subheader}</h2>
      )}
    </header>
  );
}
