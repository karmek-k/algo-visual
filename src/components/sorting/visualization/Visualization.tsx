import { useEffect, useState } from 'react';
import Blocks from './Blocks';
import { Inputs } from '../../../definitions/sorting/types';
import './Visualization.css';

type Props = {
  inputs: Inputs;
  height: number;
  width: number;
};

export default function Visualization({ inputs, height, width }: Props) {
  const [blocks, setBlocks] = useState<JSX.Element | null>(null);

  useEffect(() => {
    setBlocks(<Blocks inputs={inputs} height={height} width={width} />);
  }, [inputs]);

  return blocks;
}
