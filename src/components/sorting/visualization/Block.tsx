type Props = {
  color?: string;
  height: number;
  width: number;
};

export default function Block({ color, height, width }: Props) {
  return (
    <div
      className="visualization__blocks__block"
      style={{ height, width, backgroundColor: color }}
    />
  );
}
