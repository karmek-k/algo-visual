import { Inputs } from '../../../definitions/sorting/types';
import { useMemo } from 'react';

type Config = {
  height: number;
  width: number;
};

export default function useScalingBlocks(
  inputs: Inputs,
  { height, width }: Config
) {
  // how much do I have to multiply the tallest block's height by
  // so that it is equal to `height`?
  const vertical = useMemo<number>(() => {
    const numbers = inputs.map(input => input.number);

    return height / Math.max(...numbers);
  }, [inputs, height]);

  // how much do I have to multiply each block's width
  // so that they all fit in `width`?
  const horizontal = useMemo<number>(
    () => width / inputs.length,
    [inputs, width]
  );

  return { heightMultiplier: vertical, width: horizontal };
}
