import Block from './Block';
import { Inputs } from '../../../definitions/sorting/types';
import useScalingBlocks from './useScalingBlocks';

type Props = {
  inputs: Inputs;
  height: number;
  width: number;
};

export default function Blocks({ inputs, height, width }: Props) {
  const { width: scaledWidth, heightMultiplier } = useScalingBlocks(inputs, {
    height,
    width,
  });

  const blocks = inputs.map((input, i) => (
    <Block
      key={`${i}-${input.number}`}
      height={input.number * heightMultiplier}
      width={scaledWidth}
      color={input.color}
    />
  ));

  return <div className="visualization__blocks">{blocks}</div>;
}
