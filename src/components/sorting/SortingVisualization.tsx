import { useEffect, useState } from 'react';
import { algos, SortingAlgoData } from '../../definitions/sorting/algos';
import { Inputs } from '../../definitions/sorting/types';
import AlgoPicker from '../common/algoPicker/AlgoPicker';
import Visualization from './visualization/Visualization';

// const defaultInputs = [
//   { number: 3 },
//   { number: 9 },
//   { number: 4 },
//   { number: 1 },
//   { number: 2 },
// ];
const defaultInputs: Inputs = [];
for (let i = 0; i < 30; ++i) {
  defaultInputs.push({ number: Math.ceil(Math.random() * 100) });
}

export default function SortingVisualization() {
  const [algo, setAlgo] = useState<SortingAlgoData | null>(null);
  const [inputs, setInputs] = useState<Inputs>(defaultInputs);

  useEffect(() => {
    if (!algo) {
      return;
    }

    setInputs(defaultInputs);

    const { implementation: sort } = algo;

    // TODO: make it cleaner and improve performance
    sort(inputs, {
      update: async (values: Inputs) => {
        return new Promise(resolve =>
          setTimeout(() => {
            setInputs(values);
            resolve();
          }, 2)
        );
      },
    }).then(() =>
      setInputs(inputs.map(input => ({ color: undefined, ...input })))
    );
  }, [algo]);

  return (
    <>
      <AlgoPicker algos={algos} setAlgo={setAlgo} />
      <Visualization inputs={inputs} width={200} height={100} />
    </>
  );
}
